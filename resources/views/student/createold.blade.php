
@extends('layouts.app')


@section('content')

<div class="card">
  <div class="card-header">
    <h4>Horizontal Form</h4>
  </div>
  @php
      print_r($errors->all());
  @endphp


  <form id='fromstudent' action="{{ route('pelajar.store') }}" method="post" enctype="multipart/form-data">
    @csrf
  <div class="card-body">

    <div class="form-group row">
      <label for="id_name_student" class="col-sm-3 col-form-label">Nama</label>
      <div class="col-sm-9">
        <input   value='{{ old('name_student') }}' name='name_student' type="text" 
        class="form-control @error('name_student') is-invalid @enderror"
         id="id_name_student"
         placeholder="Email">
        @error('name_student')
            <div class='invalid-feedback'>{{ $message }}</div>
        @enderror

      </div>
    </div>

    <div class="form-group row">
      <label for="id_ic" class="col-sm-3 col-form-label">IC</label>
      <div class="col-sm-9">
        <input  name='ic' type="text" class="form-control" id="id_ic" placeholder="Email">
      </div>
    </div>

    <div class="form-group row">
      <label for="address" class="col-sm-3 col-form-label">Alamat</label>
      <div class="col-sm-9">
        <input  name='address' type="text" class="form-control" id="address" placeholder="Email">
      </div>
    </div>

    <div class="form-group row">
      <label for="id_school" class="col-sm-3 col-form-label">Sekolah</label>
      <div class="col-sm-9">
        <select name='id_school' class="form-control" id="id_school">
          <option value="" selected>Sila Pilih</option>
          @if ($lookupSchool??'')
          @foreach ($lookupSchool as $idsc=>$school)
          <option value="{{ $idsc }}">{{ $school }}</option>
          @endforeach

          @endif
      </select>
      </div>
    </div>

    
    <div class="form-group row">
      <label for="id_level" class="col-sm-3 col-form-label">Tahap</label>
      <div class="col-sm-9">
        <select name='id_level' class="form-control" id="id_level">
          <option value="" selected>Sila Pilih</option>
          @if ($lookupLevel??'')
          @foreach ($lookupLevel as $idlvl=>$level)
          <option value="{{ $idlvl }}">{{ $level }}</option>
          @endforeach

          @endif
      </select>
      </div>
    </div>


 <div class="form-group row">
      <label for="address" class="col-sm-3 col-form-label">sijil spm</label>
      <div class="col-sm-9">
        <div class="custom-file">
          <input multiple  name='sijil_spm[]' type="file" class="custom-file-input" id="customFile">
          <label class="custom-file-label" for="customFile">Choose file</label>
        </div>
      </div>
    </div>

    <div class="form-group row">
      <label for="address" class="col-sm-3 col-form-label">sijil upsr</label>
      <div class="col-sm-9">
        <div class="custom-file">
          <input name='sijil_pmr' type="file" class="custom-file-input" id="customFile">
          <label class="custom-file-label" for="customFile">Choose file</label>
        </div>
      </div>
    </div>


   
  </div>
  <div class="card-footer text-center">
    <button  data-status='simpan' type="button" class="btnSubmitStudent btn btn-primary">Simpan</button>
    <button  data-status='hantar' type="button" class="btnSubmitStudent btn btn-primary">Simpan</button>
  </div>
</form>
</div>

  @endsection

 @section('scripts')
 <script>
 $(document).on('change', '.custom-file-input', function() {

  var fileName = $(this).val().split("\\").pop();
  $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
});

</script>

  
@section('scripts')

<script>



$( document ).ready(function() {

  $( ".btnSubmitStudent" ).click(function(e) {

    e.preventDefault();

    var form=$( "#fromstudent" );

  var status=  $(this).data('status');

  var input = $("<input>")
               .attr("type", "hidden")
               .attr("name", "status").val(status);
               form.append(input);


    Swal.fire({
  title: 'Do you want to save the changes?',
  showDenyButton: true,
  showCancelButton: true,
  confirmButtonText: 'Save',
  denyButtonText: `Don't save`,
}).then((result) => {
  /* Read more about isConfirmed, isDenied below */
  if (result.isConfirmed) {

       
      Swal.fire({
       
        icon: 'info',
        title: 'sila tunggun',
        showConfirmButton: false,
     
      })

    form.submit();
  } else if (result.isDenied) {
    Swal.fire('Changes are not saved', '', 'info')
  }
})

   

});


});
</script>



    
@endsection


