@php
use App\Helpers\BankStatusHelper;
@endphp

@extends('layouts.app')


@section('content')
<div class='col-12'>
<div class="card">
    <form action="{{ route('pelajar.index') }}">

        <div class="card-body">
            <div class="form-group row">
                <label for="inputEmail3" class="col-sm-3 col-form-label">Nama</label>
                <div class="col-sm-9">
                    <input name='name_student' type="text" class="form-control" id="inputEmail3" placeholder="Nama">
                </div>
            </div>
{{-- ini utk select school --}}
            <div class="form-group row">
                <label for="id_school" class="col-sm-3 col-form-label">Sekolah</label>
                <div class="col-sm-9">
                    <select name='id_school' class="form-control" id="id_school">
                        <option value="" selected>Sila Pilih</option>
                        @if ($lookupSchool)
                        @foreach ($lookupSchool as $idsc=>$school)
                        <option value="{{ $idsc }}">{{ $school }}</option>
                        @endforeach

                        @endif
                    </select>
                </div>
            </div>
{{-- end select school --}}
            <div class="card-footer text-center">
                <button type="submit" class="btn btn-primary">cari</button>
            </div>

        </div>
    </form>
    
    <div class="card-body">
      <div class='table-responsive'>

        <div class="text-right">
            <a class="btn btn-primary" href="{{ route('pelajar.create') }}">Tambah</a>
        </div>


        <table class='table table-bordered'>
            <tr>
                <th>Bil</th>
                <th>Nama</th>
                <th>Sekolah</th>
                <th>Tahap</th>
                <th>Tindakan</th>
            </tr>
            @php
             
                $bil=BankStatusHelper::countFrom($students);
            @endphp

            @foreach ($students as $student)
            <tr>
                <td>{{ ++$bil }}</td>
                <td>{{ $student->name_student }}</td>
                <td>{{ $student->lookupSchool->name }}</td>
                <td>{{ $student->lookupLevel->name }}</td>
                <td>
                  <div class="btn-group" role="group" aria-label="Basic example">
                  <a href="{{ route('pelajar.show',['pelajar'=>$student->id]) }}" class="btn btn-primary mr-1"><i class="fas fa-file"></i></a>
                  <a href="{{ route('pelajar.edit',['pelajar'=>$student->id]) }}" class="btn btn-info mr-1"><i class="fas fa-edit"></i></a>
                  {{-- <a href="{{ route() }}" class="btn btn-danger"><i class="fas fa-trash-alt"></i></a> --}}
                  <form  method='post' action="{{ route('pelajar.destroy',['pelajar'=>$student->id]) }}">
                    @csrf
                    @method('delete')
                    <button type="submit" class="btn btn-danger"><i class="fas fa-trash-alt"></i></button>
                  </form>
                </div>
                
                </td>
            </tr>
            @endforeach

        </table>
      </div>
    </div>
      <div class='card-footer text-right'>
   
          {{ $students->appends(Request::all())->links() }}
    
        
    </div>
</div>
</div>
@endsection
