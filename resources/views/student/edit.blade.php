@extends('layouts.app')


@section('content')

<div class="card">
    <div class="card-header">
        <h4>Horizontal Form</h4>
    </div>
    @php
    print_r($errors->all());
    @endphp


@include('student._form')
    
</div>

@endsection