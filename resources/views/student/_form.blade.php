<form action="{{ $urlform }}" method="post" enctype="multipart/form-data">
    @method('put')
    @csrf
    <div class="card-body">

        <div class="form-group row">
            <label for="id_name_student" class="col-sm-3 col-form-label">Nama</label>
            <div class="col-sm-9">
                <input value='{{ old('name_student',$student->name_student??'') }}' name='name_student' type="text"
                    class="form-control @error('name_student') is-invalid @enderror" id="id_name_student"
                    placeholder="Email">
                @error('name_student')
                <div class='invalid-feedback'>{{ $message }}</div>
                @enderror

            </div>
        </div>

        <div class="form-group row">
            <label for="id_ic" class="col-sm-3 col-form-label">IC</label>
            <div class="col-sm-9">
                <input value='{{ old('ic',$student->ic??'') }}' name='ic' type="text" class="form-control"
                    id="id_ic" placeholder="Email">
            </div>
        </div>

        <div class="form-group row">
            <label for="address" class="col-sm-3 col-form-label">Alamat</label>
            <div class="col-sm-9">
                <textarea class="form-control" name="address" id="address" cols="30"
                    rows="10">{{ old('address',$student->address??'') }}</textarea>

                {{-- <input value='{{ old('address',$student->address??'') }}' name='address' type="text"
                class="form-control" id="address" placeholder="Email"> --}}
            </div>
        </div>

        <div class="form-group row">
            <label for="id_school" class="col-sm-3 col-form-label">Sekolah</label>
            <div class="col-sm-9">
                <select name='id_school' class="form-control" id="id_school">
                    <option value="" selected>Sila Pilih</option>
                    @if ($lookupSchool??'')
                    @foreach ($lookupSchool as $idsc=>$school)
                    <option {{ old('id_school',$student->id_school??'')==$idsc?'Selected':'' }} value="{{ $idsc }}">
                        {{ $school }}</option>
                    @endforeach

                    @endif
                </select>
            </div>
        </div>


        <div class="form-group row">
            <label for="id_level" class="col-sm-3 col-form-label">Tahap</label>
            <div class="col-sm-9">
                <select name='id_level' class="form-control" id="id_level">
                    <option value="" selected>Sila Pilih</option>
                    @if ($lookupLevel??'')
                    @foreach ($lookupLevel as $idlvl=>$level)
                    <option {{ old('id_level',$student->id_level??'')==$idlvl?'Selected':'' }} value="{{ $idlvl }}">
                        {{ $level }}</option>
                    @endforeach

                    @endif
                </select>
            </div>
        </div>

        <div class="form-group row">
            <label for="address" class="col-sm-3 col-form-label">sijil upsr</label>
            <div class="col-sm-9">
                <div class="custom-file">
                    <input name='sijil_pmr' type="file" class="custom-file-input" id="customFile">
                    <label class="custom-file-label" for="customFile">Choose file</label>
                </div>
                <div>
                @if ($student->ic_path)

                <a href="{{ route('downloadfile',['id'=>$student->id]) }}">download by id</a>
                <br>

                <a href="{{ route('download.path',['path'=>$student->ic_path]) }}">download by path</a>

                

                @endif
              </div>
            </div>
        </div>




    </div>

    @if ( $mode=='store')
    <div class="card-footer text-center">
        <button type="submit" class="btn btn-primary">Simpan</button>
    </div>
    @elseif($mode=='update')
    <div class="card-footer text-center">
        <button type="submit" class="btn btn-primary">Kemaskini</button>
    </div>
    @endif


   
</form>



@section('scripts')
<script>
$(document).on('change', '.custom-file-input', function() {

 var fileName = $(this).val().split("\\").pop();
 $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
});

</script>

 
@section('scripts')

<script>



$( document ).ready(function() {

 $( ".btnSubmitStudent" ).click(function(e) {

   e.preventDefault();

   var form=$( "#fromstudent" );

 var status=  $(this).data('status');

 var input = $("<input>")
              .attr("type", "hidden")
              .attr("name", "status").val(status);
              form.append(input);


   Swal.fire({
 title: 'Do you want to save the changes?',
 showDenyButton: true,
 showCancelButton: true,
 confirmButtonText: 'Save',
 denyButtonText: `Don't save`,
}).then((result) => {
 /* Read more about isConfirmed, isDenied below */
 if (result.isConfirmed) {

      
     Swal.fire({
      
       icon: 'info',
       title: 'sila tunggun',
       showConfirmButton: false,
    
     })

   form.submit();
 } else if (result.isDenied) {
   Swal.fire('Changes are not saved', '', 'info')
 }
})

  

});


});
</script>



   
@endsection
