
@extends('layouts.app')


@section('content')

<form action="{{ route('authenticate') }}" method='post'>
@csrf
  <div class="form-group row">
    <label for="email" class="col-sm-3 col-form-label">Nama</label>
    <div class="col-sm-9">
      <input   value='{{ old('email') }}' name='email' type="text" 
      class="form-control @error('email') is-invalid @enderror"
       id="email"
       placeholder="Email">
      @error('email')
          <div class='invalid-feedback'>{{ $message }}</div>
      @enderror

    </div>
  </div>

  <div class="form-group row">
    <label for="password" class="col-sm-3 col-form-label">password</label>
    <div class="col-sm-9">
      <input   value='{{ old('password') }}' name='password' type="password" 
      class="form-control @error('password') is-invalid @enderror"
       id="id_name_student"
       placeholder="password">
      @error('password')
          <div class='invalid-feedback'>{{ $message }}</div>
      @enderror

    </div>
  </div>

  <div class="card-footer text-center">
    <button type="submit" class="btn btn-primary">Login</button>
  </div>
</form>
@endsection