<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\StudentController;
use App\Models\Student;
use Illuminate\Http\Request;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('login', function () {
    return view('auth.login');
})->name('login')->middleware('guest');


Route::get('/', function () {
    return view('welcome');
});

Route::post('authenticate', [LoginController::class, 'authenticate'])->name('authenticate');

Route::post('logout', [LoginController::class, 'logout'])->name('logout');




Route::resource('pelajar', StudentController::class)->middleware('auth');

// Route::resource('senarai-pelajar', StudentController::class)->names([
//     'index' => 'pelajar.index',
//     'create' =>'pelajar.create',
//     'store' =>'pelajar.store',
  
// ]);

Route::get('ibubapa', [StudentController::class, 'editIbuBapa'])->name('maklumat.ibubapa');


// Route::get('pelajar', [StudentController::class, 'index'])->name('pelajar.senarai');

// Route::post('pelajar', [StudentController::class, 'store'])->name('pelajar.store');

// Route::get('pelajar/{id}', [StudentController::class, 'edit'])->name('pelajar.edit');

// Route::put('pelajar/{id}', [StudentController::class, 'update'])->name('pelajar.update');
// Route::delete('pelajar/{id}', [StudentController::class, 'update'])->name('pelajar.delete');


Route::get('dwonloadfile/{id}', function ($id) {
    
    $data=Student::find($id);

    return Storage::download($data->ic_path);
})->name('downloadfile');


Route::get('download-path', function (Request $request) {
   $path= $request->path;
    return Storage::download($path);
})->name('download.path');
