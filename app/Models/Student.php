<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * @property integer $id
 * @property integer $id_school
 * @property integer $id_level
 * @property string $name_student
 * @property string $ic
 * @property string $address
 * @property string $created_at
 * @property string $updated_at
 * @property Lookup $lookup
 * @property Lookup $lookup
 */
class Student extends Model
{
  
    use  SoftDeletes;
    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['ic_path','id_school', 'id_level', 'name_student', 'ic', 'address', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function lookupLevel()
    {
        return $this->belongsTo('App\Models\Lookup', 'id_level');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function lookupSchool()
    {
        return $this->belongsTo('App\Models\Lookup', 'id_school');
    }


    public function scopeMaster($query)
    {
        // return $query->where('type', $type);
    }

    
}
