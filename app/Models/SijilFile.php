<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property string $file_name
 * @property string $file_location
 * @property string $category
 * @property integer $id_reference
 * @property string $created_at
 * @property string $updated_at
 */
class SijilFile extends Model
{
    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['file_name', 'file_location', 'category', 'id_reference', 'created_at', 'updated_at'];

}
