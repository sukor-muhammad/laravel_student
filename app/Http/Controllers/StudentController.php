<?php

namespace App\Http\Controllers;

use App\Models\Lookup;
use App\Models\Student;

use App\Models\SijilFile;
use App\Mail\StandardEmail;
use Illuminate\Http\Request;

use App\Helpers\BankStatusHelper;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
       
        // dd($request);

        // dd($request);
       $school =Lookup::where(['code'=>'002','category'=>'school'])->first();

       $data=Lookup::where(['category'=>'school'])->get();

       $lookupSchool=$data->pluck('name','id');

       

       $student= Student::with(['lookupLevel','lookupSchool']);
       
  

       if($request->name_student){
        $student->where('name_student', 'like', "%$request->name_student%");
       }

       if($request->id_school){
        $student->where(['id_school'=>$request->id_school]);
       }
     

       

       $students=$student->paginate(10);

      


      $lookupSchool= BankStatusHelper::getSchool();

    


        return view('student.index',compact('students','lookupSchool'));
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $d['url']=route('pelajar.store');

        $d['lookupSchool']= BankStatusHelper::getSchool();
        $d['lookupLevel']= BankStatusHelper::getLevel();
        $d['student']=new student();

        $d['mode']='store';


        return view('student.create',$d);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

      
        // $filename='pelajar001.pdf';
        // $path='noic/sijil';
        

        // $pathnew = $request->file('sijil_pmr')->storeAs($path,$filename);
       
        //  $bil=1;
        

        // foreach($request->sijil_spm as $row){

        //     $filename='pelajartest'.++$bil.'.pdf';
          

        //     $pathnew = $row->storeAs($path,$filename);

        //     print_r($pathnew);
        // }


        
        // dd($path);

        $message=[
            'name_student.required'=>'sila isi nama pelajar'
        ];


            $validate=$request->validate([
                'name_student'=>'required',
                'ic'=>'required|unique:students,ic',
                'address'=>'required',
                'id_school'=>'required',
                'id_level'=>'required',
                'sijil_pmr'=>'required|file|mimes:pdf',
                
            ],$message);


            if($request->file('sijil_pmr')){

                $time=time();
            
            $extensionic = $request->file('sijil_pmr')->extension();
            $filename='ic_'.$time.'.'. $extensionic; 
            $path=$request->noic.'/ic';

            if($pathnew = $request->file('sijil_pmr')->storeAs($path,$filename)){
                $validate['ic_path']=$pathnew;
            }

            }

          

            // $id_staff=  Auth::id();
            
            //  $validate['umur']=$request->umur;

        //     $status= $request->status;

        //    $idstatus= Status::where(p['code'=>$status])->first();

           
     
        //         $validate['id_status']=$idstatus->id;

           
     

           

            if($data=Student::create($validate)){

                $pelajar=$data->id;


                if($request->file('sijil_pmr')){

                   
                
                $extensionic = $request->file('sijil_pmr')->extension();
                $filename='ic_'.$data->id.'.'. $extensionic; 
                $path=$request->noic.'/ic';
    
                if($pathnew = $request->file('sijil_pmr')->storeAs($path,$filename)){
            
                    $data->ic_path=$pathnew;

                    $data->save();

                }
    
                }



                // foreach($request->sijil_spm as $row){

                //     $extension = $row->extension();

                //     $filename='sijilspm'.$pelajar.'.'.$extension;
                    
                //     $path=$data->ic.'/'.'sijil/'.date('Y');
        
                   

                //     if($pathnew = $row->storeAs($path,$filename)){

                //         $SijilFile= new SijilFile();

                //             $SijilFile->file_name=$filename;
                //             $SijilFile->file_location=$pathnew;
                //             $SijilFile->category='sijil spm';
                //             $SijilFile->id_reference=$data->id;


                //             $SijilFile->save();

                        


                //     }


        
                   
                // }
        

                   
                return redirect()->route('pelajar.show',['pelajar'=>$pelajar])
                        ->with('success','Pelajar Telah di Tambah');

            }else{
                return redirect()->route('pelajar.index')
                        ->with('success','Pelajar Tidak Berjaya Ditambah');
            }

      
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $d['student']=Student::find($id);



        $data['title']='Pengguna cc';
        $data['student']=$d['student'];
        $userEmail='test@teet.com';

        $attach=asset('/stisla/assets/img/avatar/avatar-1.png');
       
 Mail::to($userEmail)->cc(['sukor@tets.com','test@test.com'])
 ->send(new StandardEmail('email.hantaremail', 
        $data,$attach));


        return view('student.show',$d);
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
     
        $d['url']=route('pelajar.update',['pelajar'=>$id]);
        $d['lookupSchool']= BankStatusHelper::getSchool();
        $d['lookupLevel']= BankStatusHelper::getLevel();

        $d['student']=Student::find($id);

        $d['mode']='update';

        return view('student.edit',$d);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validate=$request->validate([
            'name_student'=>'required',
            'ic'=>'required|unique:students,ic,'.$id.'id',
            'address'=>'required',
            'id_school'=>'required',
            'id_level'=>'required',
        ]);

    

        $data=Student::where(['id'=>$id]);

        if($request->file('sijil_pmr')){

            $time=time();
        
        $extensionic = $request->file('sijil_pmr')->extension();
        $filename='ic_'.$time.'.'. $extensionic; 
        $path=$request->noic.'/ic';

        if($pathnew = $request->file('sijil_pmr')->storeAs($path,$filename)){
            $validate['ic_path']=$pathnew;
        }

        }


        if($data->update($validate)){

            return redirect()->route('pelajar.show',['pelajar'=>$id])
            ->with('success','Pelajar Telah dikemaskini');

        }else{

            dd('test');
        }


        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Student::find($id)->delete();
  
        return redirect()->route('pelajar.index')
                        ->with('success','Pelajar Telah di Delete');
    }


    public function editIbuBapa()
    { 
        $total=1+1;

        echo $total;


        
    }

     

}
