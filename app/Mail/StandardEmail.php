<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class StandardEmail extends Mailable
{
    use Queueable, SerializesModels;
    public $view, $data, $mark, $type;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($view = '', $data = '', $attach='', $mark = '', $type = '')
    {
        if($view != '')
        $this->view = $view;

        if($data != '')
        $this->data = $data;

        if($mark != '')
        $this->mark = $mark;

        if($type != '')
        $this->type = $type;

        if($attach != '')
        $this->attach = $attach;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if($this->mark != ''){
            if($this->type != '')
                return $this->markdown($this->mark);
            else
                return $this->markdown('emails.std');
        }else if(!isset($this->data)){
            return $this->view($this->view);
        }else{
            
            return $this->view($this->view, $this->data)->attach($this->attach);
        }
        
    }
}

