<?php
namespace App\Helpers;

use App\Models\Lookup;

class BankStatusHelper{


    public static function getLevel(){

        $data=Lookup::where(['category'=>'level'])->get();

        $plucked=$data->pluck('name','id');

        return $plucked;


    }
    
   public static function getSchool(){

        $data=Lookup::where(['category'=>'school'])->get();

        $plucked=$data->pluck('name','id');

        return $plucked;


    }


    public static function countFrom($data){
        $currentPage=null;
        $perPage=null;

       
     
                    $currentPage=$data->currentPage();
                    $perPage=$data->perPage();
                    if($currentPage !=1){
                        $currentPage=$currentPage-1;
                        $startcount=$currentPage*$perPage;
                    }else{
                        $startcount=0;
                    }

              

                    return $startcount;
                    
                  
    }


}