<?php

namespace Database\Factories;

use App\Models\Student;
use Illuminate\Database\Eloquent\Factories\Factory;

class StudentFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Student::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
            $level=[1,3];
            $school=[2,4];
            $idran=rand(0,1);
            $ic=rand(111111111111,9999999999999);

        return [
            'name_student' => $this->faker->name(),
            'ic'=> $ic,
            'address' => $this->faker->address,
            'id_school' => $school[$idran],
            'id_level' =>$level[$idran],
        ];
    }
}
