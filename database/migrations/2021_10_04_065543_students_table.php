<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class StudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {
            $table->id();
            $table->string('name_student');
            $table->string('ic');
            $table->text('address');
            $table->timestamps();

            $table->unsignedBigInteger('id_school');

            $table->foreign('id_school')->references('id')->on('lookup');

            $table->unsignedBigInteger('id_level');

            $table->foreign('id_level')->references('id')->on('lookup');

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('students');

    }
}
